const express = require("express");
const router = express.Router();
const bodyparser = require("body-parser");

router.get("/", (req,res)=>{
    res.render('index.html');
})

module.exports = router; 